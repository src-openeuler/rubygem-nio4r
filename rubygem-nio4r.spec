%global gem_name nio4r
%global libev_version 4.33
Name:                rubygem-%{gem_name}
Version:             2.7.1
Release:             1
Summary:             New IO for Ruby
License:             MIT and (BSD or GPLv2+)
URL:                 https://github.com/socketry/nio4r
Source0:             https://rubygems.org/gems/nio4r-%{version}.gem
BuildRequires:       ruby(release) rubygems-devel ruby-devel rubygem(rspec) gcc
Provides:            bundled(libev) = %{libev_version}
%description
Cross-platform asynchronous I/O primitives for scalable network clients and
servers. Inspired by the Java NIO API, but simplified for ease-of-use.

%package help
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
Provides:            %{name}-doc = %{version}-%{release}
Obsoletes:           %{name}-doc < %{version}-%{release}
%description help
Documentation for %{name}.

%prep
%autosetup -n %{gem_name}-%{version} -p1

%build
gem build ../%{gem_name}-%{version}.gemspec
%global optflags %{?optflags} -fno-strict-aliasing
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/
mkdir -p %{buildroot}%{gem_extdir_mri}
cp -a .%{gem_extdir_mri}/{gem.build_complete,*.so} %{buildroot}%{gem_extdir_mri}/
rm -rf %{buildroot}%{gem_instdir}/ext/

%check
pushd .%{gem_instdir}
EV_VERSION_MAJOR=$(grep EV_VERSION_MAJOR ext/libev/ev.h | cut -d ' ' -f3)
EV_VERSION_MINOR=$(grep EV_VERSION_MINOR ext/libev/ev.h | cut -d ' ' -f3)
[ "${EV_VERSION_MAJOR}.${EV_VERSION_MINOR}" = '%{libev_version}' ]
sed -i '/require "coveralls"/ s/^/#/' spec/spec_helper.rb
sed -i '/Coveralls.wear!/ s/^/#/' spec/spec_helper.rb
rspec -I$(dirs +1)%{gem_extdir_mri} spec
popd

%files
%dir %{gem_instdir}
%{gem_extdir_mri}
%exclude %{gem_instdir}/.*
%doc %{gem_instdir}/readme.md
%license %{gem_instdir}/license.md
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files help
%doc %{gem_docdir}
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/changes.md
%{gem_instdir}/logo.png
%{gem_instdir}/rakelib
%{gem_instdir}/Rakefile
%{gem_instdir}/examples
%{gem_instdir}/nio4r.gemspec
%{gem_instdir}/spec

%changelog
* Mon May 13 2024 xu_ping <707078654@qq.com> - 2.7.1-1
- Update to 2.7.1

* Wed Nov 15 2023 wangkai <13474090681@163.com> - 2.5.9-1
- Update to 2.5.9

* Tue Aug 23 2022 wangkai <wangkai385@h-partners.com> - 2.5.2-1
- Update to 2.5.2

* Mon Aug 24 2020 liyanan <liyanan32@huawei.com> - 2.3.1-1
- package init
